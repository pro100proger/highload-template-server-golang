package main

import (
	"config"
	"db"
	"handler"
	"log"
	"logger"
	"net/http"
	"os"
	"runtime"
)

var (
	logFp *os.File
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	if len(os.Args) < 2 {
		log.Fatal("Usage: main <port>")
	}
	var err error
	logFile := config.Logfile

	if logFp, err = os.OpenFile(logFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666); err != nil {
		log.Fatal("Cannot open log file: ", err.Error())
	}

	logger.Init(logFp, logFp, logFp, logFp)

	dispatcher := handler.NewDispatcher(config.Handle_workers)
	dispatcher.Run()

	http.HandleFunc("/", handler.HighloadHandler)

	logger.LogInfo("Server start on port " + os.Args[1])

	http.ListenAndServe(":"+os.Args[1], nil)
}
