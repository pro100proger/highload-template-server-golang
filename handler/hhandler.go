package handler

import (
	// "github.com/pquerna/ffjson/ffjson"
	// "handler/datas"
	// "io/ioutil"
	"logger"
	"net/http"
)

func HighloadHandler(w http.ResponseWriter, r *http.Request) {

	/* TODO: pre-processing and validation of requests may be here, next sample show how to send request to main process function:
	JobQueue <- Job{Payload: value}
	*/

	/* Usage example:
	b, _ := ioutil.ReadAll(r.Body) // Read bytes

	x := new(datas.JSONProfile) // Structure to decode request

	jsonFastDecoder := ffjson.NewDecoder()
	err := jsonFastDecoder.Decode(b, x)
	if err != nil {
		logger.LogError("Cannot decode bytes " + err.Error() + "\n" + string(b))
		// Cannot decode -> bad response
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusBadRequest)
	} else {
		for _, values := range *x {
			work := Job{Payload: values}
			// give payload to workers
			JobQueue <- work
		}
		// Good response if request is valid
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusOK)
	}
	*/
}
