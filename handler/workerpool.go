package handler

import (
	"datas"
	"gopkgs.com/geoip.v1"
	"logger"
	"time"
)

var (
	MaxWorker = 20
	MaxQueue  = 20
)

// Job represents the job to be run
type Job struct {
	Payload datas.JProfile
}

// A buffered channel that we can send work requests on.
var JobQueue chan Job

// Worker represents the worker that executes the job
type Worker struct {
	WorkerPool chan chan Job
	JobChannel chan Job
	quit       chan bool
}

func NewWorker(workerPool chan chan Job) Worker {
	return Worker{
		WorkerPool: workerPool,
		JobChannel: make(chan Job),
		quit:       make(chan bool)}
}

// Start method starts the run loop for the worker, listening for a quit channel in
// case we need to stop it
func (w Worker) Start() {
	go func() {
		Geodb, err := geoip.Open("GeoLite2-Country.mmdb.gz")
		if err != nil {
			logger.LogError(err.Error())
		}
		for {
			// register the current worker into the worker queue.
			w.WorkerPool <- w.JobChannel
			select {
			case job := <-w.JobChannel:
				/*
					TODO:
					In this case you may override job-processing
					or you may create a function that will processing
					requests
				*/
				(&w).OneJobProcess(&job, Geodb)

			case <-w.quit:
				// we have received a signal to stop
				return
			}
		}
	}()
}

// Stop signals the worker to stop listening for work requests.
func (w Worker) Stop() {
	go func() {
		w.quit <- true
	}()
}
