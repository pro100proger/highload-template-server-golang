package logger

import (
	"config"
	"io"
	"log"
)

type Logs struct {
	Code string
	Msg  string
}

var (
	Trace   *log.Logger
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
	LCH     chan Logs
)

func Init(
	traceHandle io.Writer,
	infoHandle io.Writer,
	warningHandle io.Writer,
	errorHandle io.Writer) {

	Trace = log.New(traceHandle,
		"TRACE: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Info = log.New(infoHandle,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Warning = log.New(warningHandle,
		"WARNING: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Error = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	LCH = make(chan Logs, 5)
	go simpleQueuedLogger()
}

func simpleQueuedLogger() {
	for {
		w := <-LCH
		switch w.Code {
		case "Info":
			Info.Println(w.Msg)
		case "Warning":
			Warning.Println(w.Msg)
		case "Trace":
			Trace.Println(w.Msg)
		case "Error":
			Error.Println(w.Msg)
		}
	}
}

func LogError(toLog string) {
	LCH <- Logs{Code: "Error", Msg: toLog}
}

func LogInfo(toLog string) {
	if config.Loglevel == "info" {
		LCH <- Logs{Code: "Info", Msg: toLog}
	}
}

func LogTrace(toLog string) {
	if config.Loglevel == "info" || config.Loglevel == "trace" {
		LCH <- Logs{Code: "Trace", Msg: toLog}
	}
}

func LogWarning(toLog string) {
	if config.Loglevel == "info" || config.Loglevel == "warning" || config.Loglevel == "trace" {
		LCH <- Logs{Code: "Warning", Msg: toLog}
	}
}
